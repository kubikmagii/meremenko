package ml.antanaks.school.base;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

public class NumberOperations {

    public static Integer find(int[] array, int value) {
        for(int i=0; i < array.length; i++) {
            if (array[i] == value) {
                return i;
            }
        }
        return null;
    }

    public static Integer find(double[] array, double value, double eps) {
        double minV = value - eps;
        double maxV = value + eps;
        for (int i = 0; i < array.length; i++) {
            if(Math.abs(array[i])>=minV && Math.abs(array[i])<=maxV) {
                return i;
            }
        }
        return null;
    }

    public static Double calculateDensity(double weight, double volume, double min, double max) {
        if (weight/volume >= min && weight/volume <= max) {
            return weight/volume;
        }
        return null;
    }

    public static Integer find(BigInteger[] array, BigInteger value) {
        for(int i=0; i < array.length; i++) {
            if (Objects.equals(array[i], value)) {
                return i;
            }
        }
        return null;
    }

    public static BigDecimal calculateDensity(BigDecimal weight, BigDecimal volume, BigDecimal min, BigDecimal max) {
        if (weight.divide(volume).compareTo(min)==1 && weight.divide(volume).compareTo(max)==-1) {
            return weight.divide(volume);
        }
        return null;
    }
}
