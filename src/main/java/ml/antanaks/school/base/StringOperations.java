package ml.antanaks.school.base;

import java.text.DecimalFormat;
import java.util.Arrays;

public class StringOperations {

    public static int getSummaryLength(String[] strings) {
        int sum = 0;
        for (String str : strings) {
            sum += str.length();
        }
        return sum;
    }

    public static String getFirstAndLastLetterString(String string) {
        char[] firstAndLast = {string.charAt(0), string.charAt(string.length() - 1)};
        String stringFirstAndLast = new String(firstAndLast);
        return stringFirstAndLast;
    }

    public static boolean isSameCharAtPosition(String string1, String string2, int index) {
        return string1.charAt(index) == string2.charAt(index);
    }

    public static boolean isSameFirstCharPosition(String string1, String string2, char character) {
        return string1.indexOf(character) == string2.indexOf(character);
    }

    public static boolean isSameLastCharPosition(String string1, String string2, char character) {
        return string1.lastIndexOf(character) ==
                string2.lastIndexOf(character);
    }

    public static boolean isSameFirstStringPosition(String string1, String string2, String str) {
        return string1.indexOf(str) == string2.indexOf(str);
    }

    public static boolean isSameLastStringPosition(String string1, String string2, String str) {
        return string1.lastIndexOf(str)
                == string2.lastIndexOf(str);
    }

    public static boolean isEqual(String string1, String string2) {
        return string1.equals(string2);
    }

    public static boolean isEqualIgnoreCase(String string1, String string2) {
        return string1.equalsIgnoreCase(string2);
    }

    public static boolean isLess(String string1, String string2) {
        return string1.compareTo(string2)<0;
    }

    public static boolean isLessIgnoreCase(String string1, String string2) {
        return isLess(string1.toLowerCase(), string2.toLowerCase());
    }

    public static String concat(String string1, String string2) {
        return string1.concat(string2);
    }

    public static boolean isSamePrefix(String string1, String string2, String prefix) {
        return string1.startsWith(prefix) && string2.startsWith(prefix);
    }

    public static boolean isSameSuffix(String string1, String string2, String suffix) {
        return string1.endsWith(suffix) == string2.endsWith(suffix);
    }

    public static String getCommonPrefix(String string1, String string2) {
        StringBuilder prefix = new StringBuilder();
        String indLength;
        if (string1.length() > string2.length()) {
            indLength = string2;
        } else {
            indLength = string1;
        }

        for (int i = 0; i < indLength.length(); i++) {
            if(!isSameCharAtPosition(string1,string2, 0)){
                return prefix.toString();
            }
            if (isSameCharAtPosition(string1, string2, i)) {
                prefix.append(indLength.charAt(i));
            }
        }
        return prefix.toString();
    }

    public static String reverse(String string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        return stringBuilder.reverse().toString();

    }

    public static boolean isPalindrome(String string) {
        return isEqual(string, reverse(string));
    }

    public static boolean isPalindromeIgnoreCase(String string) {
        return isEqualIgnoreCase(string, reverse(string));
    }

    public static String getLongestPalindromeIgnoreCase(String[] strings) {
        String longestPalindrome = "";
        for (String s: strings) {
            if (isPalindromeIgnoreCase(s)) {
                if (s.length()>longestPalindrome.length()) {
                    longestPalindrome = s;
                }
            }
        }
        return longestPalindrome;
    }

    public static boolean hasSameSubstring(String string1, String string2, int index, int length){
        int howLong = Math.min(string1.length(), string2.length());
        if(index+length <= howLong){
            return isEqual(string1.substring(index, index + length - 1), string2.substring(index, index + length - 1));
        }
        return false;
    }

    public static boolean isEqualAfterReplaceCharacters(String string1, char replaceInStr1, char replaceByInStr1,
                                                        String string2, char replaceInStr2, char replaceByInStr2) {
        return isEqual(string1.replace(replaceInStr1,replaceByInStr1), string2.replace(replaceInStr2,replaceByInStr2));
    }

    public static boolean isEqualAfterReplaceStrings(String string1, String replaceInStr1, String replaceByInStr1,
                                                     String string2, String replaceInStr2, String replaceByInStr2) {
        return isEqual(string1.replaceAll(replaceInStr1, replaceByInStr1), string2.replaceAll(replaceInStr2, replaceByInStr2));
    }

    public static boolean isPalindromeAfterRemovingSpacesIgnoreCase(String string){
        return isPalindromeIgnoreCase(string.replaceAll("\\s", ""));
    }

    public static boolean isEqualAfterTrimming(String string1, String string2) {
        return isEqual(string1.trim(), string2.trim());
    }

    public static String makeCsvStringFromInts(int[] array) {
    String str = Arrays.toString(array);
    return str.substring(1, str.length()-1).replace(" ","");
    }

    public static String makeCsvStringFromDoubles(double[] array) {
        if (array.length!=0) {
            String out = "";
            DecimalFormat df = new DecimalFormat("#.00");
            for (Double d : array) {
                out += df.format(d) + ",";
            }
            return out.substring(0, out.length() - 1);
        }
        return "";
    }

    public static StringBuilder makeCsvStringBuilderFromInts(int[] array) {
        StringBuilder str = new StringBuilder();
        return str.append(makeCsvStringFromInts(array));
    }

    public static StringBuilder makeCsvStringBuilderFromDoubles(double[] array) {
        StringBuilder str = new StringBuilder();
        return str.append(makeCsvStringFromDoubles(array));
    }

    public static StringBuilder removeCharacters(String string, int[] positions) {
        StringBuilder sb = new StringBuilder(string);
        for(int i = positions.length-1; i>=0; i--){
            sb.deleteCharAt(positions[i]);
        }
        return sb;
    }

    public static StringBuilder insertCharacters(String string, int[] positions, char[] characters) {
        StringBuilder sb = new StringBuilder(string);
        for (int i = positions.length-1; i>=0; i--) {
            sb.insert(positions[i], characters[i]);
        }
        return sb;
    }

}
